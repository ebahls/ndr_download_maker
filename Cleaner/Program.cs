﻿using log4net;
using NDRDownloadMaker;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cleaner
{
    class CleanerProg
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(CleanerProg));

        static void Main(string[] args)
        {
            log.Info("StartCleanup Process");
            Config conf= HelperFacade.loadConfig(log);
            log.InfoFormat("Max File Retention {0} tage", conf.retention);

            bool delete = true;
            if (conf.retention == 0)
            {
                delete = false;
            }
            log.InfoFormat("Check Download Pfad {0}", conf.downloadPath);
           
            cleanPath(conf.downloadPath,conf.retention, false,delete);
            log.InfoFormat("Check TMP Pfad {0}", conf.tmpPath);
            cleanPath(conf.tmpPath, conf.retention, true, delete);


        }

        public static void cleanPath(string path,int retention,bool directory,bool delete)
        {
            if (directory)
            {
                foreach (string filename in Directory.GetDirectories(path))
                {
                    DirectoryInfo fi = new DirectoryInfo(filename);


                    double days = DateTime.Today.Subtract(fi.CreationTime).TotalDays;
                    log.InfoFormat("chek directory {0} days old {1}", filename, days);

                    if (days>=retention)
                    {
                        log.Info("Delete Directory");
                        if (delete)
                        {
                            try
                            {
                                Directory.Delete(filename, true);
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex);
                            }
                        }
                    }

                }

            }
            else
            {
                foreach (string filename in Directory.GetFiles(path))
                {
                    FileInfo fi = new FileInfo(filename);

                    double days = DateTime.Today.Subtract(fi.CreationTime).TotalDays;
                    log.InfoFormat("chek file {0} days old {1}", filename,days);
                    if (days >= retention)
                    {
                        log.Info("Delete File");
                        if (delete)
                        {
                            try
                            {
                               File.Delete(filename);
                            }
                            catch (Exception ex)
                            {
                                log.Error(ex);
                            }
                        }
                    }

                }
                
               
            }
        }
    }
}
