﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Ionic.Zip;
using log4net;
using Microsoft.Win32;
using NDRDownloadMaker.HypService;
using NDRDownloadMaker.HypServiceStream;
using System.Globalization;

namespace NDRDownloadMaker
{
    public class DownloadMaker
    {
        string token;
        HypService.HypServiceClient client;
        HypServiceStream.HypServiceStreamClient sClient;
        List<DCDocIdent> docReqList;
        public List<DownloadContent> content { get; set; }
        public string user { get; set; }
        private ILog log;
        private Dictionary<string, Tool> toolDictonary;
        public string mailAdresse { get; set; }
        private Config conf;
        private DateTime version_date;
        private long downloded_filesize = 0;

        public DownloadMaker(Config conf,string token, ILog log)
        {
            this.token = token;
            this.client = new HypService.HypServiceClient();
            this.sClient = new HypServiceStream.HypServiceStreamClient();
           
            docReqList = new List<DCDocIdent>();
            this.conf = conf;

            log.Info("create with token:" + token);
            this.log = log;

            GetTrusteeInfoRequest req = new GetTrusteeInfoRequest();
            req.sUserToken = token;
            req.sRequestID = "44";

            GetTrusteeInfoResponse resp= client.GetTrusteeInfo(req);
            user = resp.Trustee.sDisplayName;
            log.Info("User :" + user);
            mailAdresse = "-";
            foreach (MailUser muser in conf.mailconfig.mapping)
            {
                if (string.Compare(muser.user,user,true)==0)
                {
                    log.Info("using mail adress :" + muser.mailadress);
                    this.mailAdresse = muser.mailadress;
                    break;
                }
            }
        }

        public bool setVersionDate(string date)
        {
            string pattern = "dd.MM.yyyy";
            log.Debug("parese Date :" + date);
            return DateTime.TryParseExact(date, pattern, null, DateTimeStyles.None, out version_date);
        }

        public void addDocument(int dbID,int docID)
        {
            DCDocIdent docIdent = new DCDocIdent();
            docIdent.uiDdbID = (uint) dbID;
            docIdent.uiDocID = (uint) docID;
            this.docReqList.Add(docIdent);
            log.Debug("add document :" + docID);
        }

        public void collectAllFromStamp(string stampname,string fieldname,string value)
        {
            // build query
            log.Info("take from stamp:"+stampname);
            string query = value + "*";

            PerformQueryRequest req = new PerformQueryRequest();
            req.sRequestID = "43";
            req.sUserToken = token;
            req.sStampName = stampname;
            req.sDomain = "*";

            log.Debug("query :" + query);

            HypService.DCFieldValue fv = new HypService.DCFieldValue()
            {
                sFieldName = fieldname,

                sFieldValue = query
            };
            req.FieldValues = new List<HypService.DCFieldValue>();
            req.FieldValues.Add(fv);

            PerformQueryResponse resp = this.client.PerformQuery(req);
            if (resp.iErrorID != 0)
            {
                log.Error("ERROR WCF errorID:" + resp.iErrorID);
                return;
            }
            this.docReqList.Clear();

            foreach (DCDocIdent dcIdent in resp.DocIdents)
            {
                this.addDocument((int)dcIdent.uiDdbID, (int)dcIdent.uiDocID);
            }
            log.Debug("rebuild result");
            processRequestPrep();

        }

        

        
        public void singleReqFolder(DownloadContent content)
        {
            // build query
            string query = content.originalPath + "|" + content.originalPath + "\\*";

            PerformQueryRequest req = new PerformQueryRequest();
            req.sRequestID = "43";
            req.sUserToken = token;
            req.sStampName = "dg file";
            req.sDomain = "*";

            log.Debug("query :" + query);

            HypService.DCFieldValue fv = new HypService.DCFieldValue()
            {
                sFieldName = "PATH",

                sFieldValue = query
            };
            req.FieldValues = new List<HypService.DCFieldValue>();
            req.FieldValues.Add(fv);

            PerformQueryResponse resp=this.client.PerformQuery(req);
            if (resp.iErrorID!=0)
            {
                log.Error("ERROR WCF errorID:" + resp.iErrorID);
                return;
            }
            this.docReqList.Clear();
            foreach(DCDocIdent dcIdent in resp.DocIdents)
            {
                this.addDocument((int) dcIdent.uiDdbID, (int) dcIdent.uiDocID);
            }
            log.Debug("rebuild result");
            processRequestPrep();

            

        }

        private bool downloadFile(uint dbID, uint docID, string path)
        {
            SDocGetDataStreamRequest request = new SDocGetDataStreamRequest();
            request.sUserToken = token;
            request.uiDdbID = dbID;
            request.uiDocID = docID;

           
            log.Debug("compare " + DateTime.Today + " with " + this.version_date);
            bool ignore = true;
            if (DateTime.Compare(this.version_date, DateTime.Today) >=0)
            {
                log.Info("using latest Version");
                ignore = false;

            }
            else
            {
                log.Info("using version from " + this.version_date);
                MDocGetInfoRequest infoRequest = new MDocGetInfoRequest();
                infoRequest.sUserToken = this.token;
                infoRequest.sRequestID = "12";

                DCDocIdent docid = new DCDocIdent();
                docid.uiDdbID = dbID;
                docid.uiDocID = docID;
                infoRequest.RequestList = new List<DCDocIdent>();
                infoRequest.eInfoFlags = DocInfoFlags.DocInfoHistory;
                infoRequest.RequestList.Add(docid);

                MDocGetInfoResponse resp= client.MDocGetInfo(infoRequest);
                
                foreach (DCDocInfo inf in resp.DocInfoList)
                {
                     foreach(DCInstance versions in inf.InfoHistory.Instances)
                    {
                        log.Debug("Versions Date:" + versions.CreationDate);
                        log.Debug("Version :" + versions.uiVersionNumber);

                        if (versions.CreationDate.CompareTo(this.version_date)<=0)
                        {
                            log.Debug("DocID " + versions.uiDocID + " matches");
                            request.uiDocID = versions.uiDocID;
                            ignore = false;
                        }
                         
                    }
                }

            }
            SDocGetDataStreamResponse response = null;
            if (ignore==false)
            {
                  response = sClient.SDocGetDataStream(request);
            }
            else
            {
                log.Debug("Ignore File it is in the future");
            }
                
            if (response!=null && response.oStream != null   )
            {
                FileStream outstream = File.Open(path, FileMode.Create, FileAccess.Write);

                const int bufferLen = 4096;
                byte[] buffer = new byte[bufferLen];
                int count = 0;
                int bytecount = 0;
                while ((count = response.oStream.Read(buffer, 0, bufferLen)) > 0)
                {
                    outstream.Write(buffer, 0, count);
                    Console.Write(".");
                    bytecount += count;
                    this.downloded_filesize += count;
                }
                outstream.Close();
                response.oStream.Close();
                return true;
            }

            return false;
        }

        public string toZipFile(string root,string downloadName)
        {
            string uid = Guid.NewGuid().ToString();
            string tmpPath = Path.Combine(conf.tmpPath, uid);
            Directory.CreateDirectory(tmpPath);
            using (ZipFile zip = new ZipFile())
            {
                foreach (DownloadContent cn in this.content)
                {
                    Console.Write("docID {0}", cn.docID);
                    string tmpFile = Path.Combine(tmpPath, cn.tmpFilename);
                    if (this.downloadFile(cn.dbID, cn.docID, tmpFile))
                    {
                        try
                        {
                            ZipEntry e = zip.AddFile(tmpFile);

                            Console.WriteLine("add {0}", (cn.zipPath(root) + cn.originalName));
                            string filename = cn.zipPath(root) + cn.originalName;
                            log.Debug("add:" + filename);
                            e.FileName = filename;
                        }
                        catch(Exception ex)
                        {
                            log.Error("Zip Exception ", ex);
                        }
                    }
                }

                zip.Save(Path.Combine(conf.downloadPath,downloadName));

                return conf.downloadbaseURL + "/" + downloadName;
            }
        }

        public async void zipAndShipTool( string mailAddr, bool toolTest = false)
        {

            List<string> urls = new List<string>();

            this.processRequestPrep(toolTest);
            List<Tool> tools = this.getInvolvedTools();
            foreach (Tool t in tools)
            {
                this.collectAllFromStamp(t.stamp, t.field, t.objectID);

                DateTime date = DateTime.Now;
                string fileName = date.ToString("HH-mm-ss_dd_MM_yyyy") + "_" + t.objectID + "_" + t.name + ".zip";
                string url = this.toZipFile(this.getRoot(), fileName);
                log.Info(url);
                urls.Add(url);
            }

            HelperFacade.sendMail(mailAddr, conf, urls, this.log);
        }

        public async void zipAndShipSelected( string mailAddr)
        {

            List<string> urls = new List<string>();

           

            this.processRequestPrep(false);





            DateTime date = DateTime.Now;
            string fileName = date.ToString("HH-mm-ss_dd_MM_yyyy") + "_Verzeichnis.zip";
            string url = this.toZipFile(this.getRoot(), fileName);
            log.Info(url);
            urls.Add(url);

            HelperFacade.sendMail(mailAddr, conf, urls, log);
        }

        public void zipAndShipRecursive( string mailAddr)
        {

            List<string> urls = new List<string>();



            this.processRequestPrep(false);

            this.singleReqFolder(this.content[0]);



            DateTime date = DateTime.Now;
            string fileName = date.ToString("HH-mm-ss_dd_MM_yyyy") + "_Verzeichnis.zip";
            string url = this.toZipFile(this.getRoot(), fileName);
            log.Info(url);
            urls.Add(url);

            HelperFacade.sendMail(mailAddr, conf, urls, log);
        }

        public string getRoot()
        {
            string root="";
            int components=0;
            foreach (DownloadContent cn in this.content)
            {
                int c = cn.getPathComponents().Length;
                if (components==0)
                {
                    components = c;
                    root = cn.originalPath;

                }else if (components>c)
                {
                    root = cn.originalPath;
                    components = c;
                }
            }
            log.Debug("Rootp Path:" + root);
            return root;
        }

        public void processRequestPrep(bool toolTest=false)
        {
            this.downloded_filesize = 0;
            this.toolDictonary = new Dictionary<string, Tool>();
            HypService.MDocGetIndexRequest req = new HypService.MDocGetIndexRequest();
            req.sRequestID = "42";
            req.sStampName = "dg file";
            req.sUserToken = this.token;
            req.RequestList = this.docReqList;
            int count = 0;
            HypService.MDocGetIndexResponse resp = client.MDocGetIndex(req);
            this.content = new List<DownloadContent>();
            foreach (List<HypService.DCFieldValue> fieldValues in resp.Indexes)
            {
              
                DownloadContent content = new DownloadContent(this.docReqList[count].uiDdbID, this.docReqList[count].uiDocID);
                foreach(HypService.DCFieldValue fv in fieldValues)
                {
                     
                    if (string.Equals(fv.sFieldName, "filename", StringComparison.OrdinalIgnoreCase))
                    {
                        content.originalName = fv.sFieldValue;

                    }else if (string.Equals(fv.sFieldName, "path", StringComparison.OrdinalIgnoreCase))
                    {
                        content.originalPath = fv.sFieldValue;
                    }

                    content.tmpFilename = "tmp_" + count + ".tmp";
 
                       
                }
                count++;
                if (toolTest)
                {
                    Tool tool=HelperFacade.evaluateToolUse(conf, content);
                    if (tool!=null)
                    {
                        log.Debug("found tool :" + tool.name+ "toolID: "+tool.objectID);
                        if (!this.toolDictonary.ContainsKey(tool.objectID))
                        {
                            this.toolDictonary.Add(tool.objectID, tool);
                        }
                            
                    
                    }

                }
                this.content.Add(content);
            }
        }

        public List<Tool> getInvolvedTools()

        {
            return this.toolDictonary.Values.ToList();
        }

         

    }

    public class  DownloadContent
    {
        public string tmpFilename { get; set; }
        public string originalPath { get; set; }
        public string originalName { get; set; }
        public uint dbID { get; set; }
        public uint docID { get; set; }
        private  string[] components;

        public DownloadContent(uint dbID,uint docID)
        {
            this.dbID = dbID;
            this.docID = docID;
        }
        public DownloadContent(string path,string filename,string tmpFilename)
        {
            this.originalName = filename;
            this.originalPath = path;
            this.tmpFilename = tmpFilename;
        }

        public string zipPath(string rootPath)
        {
            
            int rootPathLengt = rootPath.Length;
            string rootpath= this.originalPath.Substring(rootPathLengt);
            if (!rootpath.EndsWith("\\"))
            {
                rootpath = rootpath + "\\";
            }
            return rootpath;
        }

        public static string getExtention(string name)
        {
            string[] comp=name.Split('.');
            if (comp.Length == 1) return null;
            return comp[comp.Length - 1];
        }

        public Tool toolExtentionMatch(Tool tool)
        {
            Tool response = null;
            foreach (string e in tool.extentions)
            {
                string ext = e.ToUpper();

                if (this.originalName.ToUpper().EndsWith(ext))
                {
                    return HelperFacade.makeToolInstance(tool, this.originalName, ext);
                }
                else
                {
                    foreach (string comp in this.getPathComponents())
                    {
                        if (comp.ToUpper().EndsWith(ext))
                        {
                            return HelperFacade.makeToolInstance(tool, comp,ext);
                        }

                    }
                }
            }
            return response;
        }

        public string[] getPathComponents()
        {
           return this.originalPath.Split('\\');
            
        }

        public string ToString()
        {
            return this.originalPath + " -- " + this.originalName + " --- tmp " + this.tmpFilename;
        }

         
    }

    public class HelperFacade
    {
        public static string getUserToken(string user,string domain,string pwd)
        {
            HypRegService.RegisterWindowsUserRequest request = new HypRegService.RegisterWindowsUserRequest();

            request.sDomain = domain;
            request.sUsername = user;
            request.sPassword = pwd;
            request.sApplicationID = "common";
            request.sRequestID = "42";
           
             
            HypRegService.HypRegServiceClient client = new HypRegService.HypRegServiceClient();
            HypRegService.RegisterWindowsUserResponse resp=client.RegisterWindowsUser(request);
            return resp.sUserToken;

            
        }
        public static Config loadConfig(ILog log)
        {
            string configPath=(string) Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\ACS", "NDRExtentionPath", @"C:\tmp\extentionconf.xml");

            log.Info("load config from " + configPath);

            if (!File.Exists(configPath))
            {
                Config conf = new Config();
                conf.tmpPath = @"C:\tmp";
                conf.downloadPath = @"C:\Download";
                conf.downloadbaseURL = "http://localhost/download/";

                // Tool exsample setup
                Tool eplan = new Tool();
                eplan.extentions = new string[] { ".edb", ".edk" };
                eplan.name = "Eplan";
                eplan.stamp = "Eplan";
                eplan.field = "number";

                Tool microstation = new Tool();
             
                microstation.extentions = new string[] { ".dgn", ".geb" };
                microstation.name = "Microstation";
                microstation.stamp = "microstation";
                microstation.field = "number";

                conf.tools = new Tool[] { eplan, microstation };

                MailConfig mailConfig = new MailConfig();

                MailUser user1 = new MailUser();
                user1.user = "test\\user1";
                user1.mailadress = "mustermann@test.de";

                MailUser user2 = new MailUser();
                user2.user = "test\\user2";
                user2.mailadress = "mustermann2@test.de";

                mailConfig.server = "mailserver";
                mailConfig.port = 25;
                mailConfig.pwd = "secret";
                mailConfig.absender = "server@test.de";
                mailConfig.user = "mailuser";
                mailConfig.ssl = true;
                mailConfig.subject = "Dwnload Fertig";
                mailConfig.bcc = "test@test.com";
                

                mailConfig.mapping = new MailUser[] { user1, user2 };

                conf.mailconfig = mailConfig;

                XmlSerializer ser = new XmlSerializer(typeof(Config));
                
                TextWriter writer = new StreamWriter(configPath);
                ser.Serialize(writer, conf);
                writer.Close();
                return conf;
            }
            else
            {
                try
                {
                    Config conf; 
                    StreamReader xmlStream = new StreamReader(configPath);
                    XmlSerializer serializer = new XmlSerializer(typeof(Config));
                    conf= (Config)serializer.Deserialize(xmlStream);
                    return conf;
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }

            }

            return null;
        }
        public static Tool evaluateToolUse(Config conf,DownloadContent content)
        {
            foreach (Tool tool in conf.tools)
            {

                Tool respTool = content.toolExtentionMatch(tool);
                if (respTool!=null) return respTool;
                    
             } 
                
            
            return null;
        }
        public static Tool makeToolInstance(Tool tool,string rawID,string ext)
        {
            Tool resp = new Tool();
            resp.name = tool.name;
            resp.stamp = tool.stamp;
            resp.field = tool.field;
            resp.objectID=rawID.Substring(0,rawID.Length-ext.Length);
            return resp;
        }
        public static void sendMail(string mailadress, Config conf,List<string> links,ILog log)
        {
            if (conf.mailconfig != null)
            {
                MailConfig mc = conf.mailconfig;

                SmtpClient smtpClient = new SmtpClient();

                log.Debug("using [" + mc.server + ":" + mc.port + "]");
                smtpClient.Host = mc.server;
                smtpClient.Port = mc.port;

                log.Debug("Enable SSL " + mc.ssl);
                smtpClient.EnableSsl = mc.ssl;
                smtpClient.UseDefaultCredentials = false;
                if (mc.pwd!= null)
                {
                    log.Debug("Use Credentials user:[" + mc.user + "] pwd :[******]");
                    NetworkCredential cr = new NetworkCredential(mc.user, mc.pwd);
                    smtpClient.Credentials = cr;
                }

                // evaluate body

                string body = "Sie können Die Angeforderten Dateien "+conf.retention +"Tage unter \n ";
                foreach(string url in links)
                {
                    body += url + " \n";
                }

                body += "\n Downloaden" ;
                string subject = conf.mailconfig.subject + " " + "Download Anfrage von " + DateTime.Now;
                using (var msg = new System.Net.Mail.MailMessage(conf.mailconfig.absender,mailadress,subject , body))
                {


                    try
                    {
                        if (conf.mailconfig.subject!=null)
                        {
                            MailAddress Bcopy = new MailAddress(conf.mailconfig.bcc);
                            msg.Bcc.Add(Bcopy);
                        }
                        smtpClient.Send(msg);

                    }
                    catch (Exception ex)
                    {
                        log.Error("Could not send Email", ex);
                    }
                }

            }
            else
            {
                log.Warn("No mail configuration available");
            }
        }

    }

    public class Config
    {
        public string tmpPath { get; set; }
        public int retention { get; set; }
        public string downloadPath { get; set; }
        public string downloadbaseURL { get; set; }
        public Tool[] tools { get; set; }
        public MailConfig mailconfig { get; set; }

    }

  

    public class Tool
    {
        public string name { get; set; }
        public string stamp { get; set; }
        public string field { get; set; }
        [XmlArrayItem("ext")]
        public string[] extentions { get; set; }

        [XmlIgnore]
        public string objectID { get; set; }



    }

    public class MailConfig
    {
        public string server { get; set; }
        public int port { get; set; }
        public string pwd { get; set; }
        public string user { get; set; }
        public bool  ssl { get; set; }
        public string absender { get; set; }
        public string bcc { get; set; }
        public string subject { get; set; }

        [XmlElement("MailUser")]
        public MailUser[] mapping { get; set; }
}

    public class MailUser
    {
        [XmlAttribute]
        public string user { get; set; }
        [XmlAttribute]
        public string mailadress { get; set; }
    }

}
